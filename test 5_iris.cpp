#include<math.h>
#include "EasyBMP.h"  //8584811623
#include<iostream.h>
#include<process.h>
int whitecalculator(int n,int wc,int i);
int blackcalculator(int n,int bc,int i);
void point_patch(int img[1000][1000],int arr[1000][1000],int x,int y,int flag,int ref_point);
void point_patch_layer(int img[1000][1000],int arr[1000][1000],int x,int y,int flag,int ref_point);
void patch_layer(int img[1000][1000],int arr[1000][1000],int x,int y,int flag);
void patch(int img[1000][1000],int arr[1000][1000],int x,int y,int flag);
void reset_array(int rect_image[1000][1000],int image[1000][1000],int para[1000][1000],int r,int c);
struct store
{ int x,y,wc,bc;};
int rect_image[1000][1000],para[1000][1000],image[1000][1000] ,illusion[1000][1000],opt,flag1,r,c,i,mod,j,x,k,n,m,diff1,diff2,patchsize,patchnum1,patchnum2,p,sum_wc,sum_bc;//point array index counter
  store point[1000000] ;int ival[1000000],sum,wc[1000000],bc[1000000],sym[1000000],count[1000000],avg[1000000],length[1000000];
  int flagconv_1a = 0, flagconv_1b = 0, flagconv_3 = 0, flagconv_4 =0, choice=0; int shadow[1000][1000],shadow_layer[1000][1000]; int flag2=1;
  int point_opt,pt_x,pt_y;
main ()
{
  BMP RlImg,IllImg;
  cout << "Simulation of White's illusion vs Simultaneous Brightness Contrast illusion.\n";
  cout << "\nMenu: \n1. White's Illusion (patch size - 2)\n2. Manual input (utilize for testing other patch sizes)\n3. SBC illusion\n4. SBC (second approach)\n5. Manual entry (alpha)\n6. Image Read\nEnter your choice: ";
  cin >> opt;
  switch(opt)
{ case 1 : {r=9;c=10,n=100;
  for(i=1;i<c+1;i++)
  image[1][i]=0;
  for(j=2;j<r+1;j++)
   {  for(k=1;k<c+1;k++)
      {    if(j%2==0)
           {   if(k==3||k==4)
               image[j][k]=n;
               else
               image[j][k]=255;
           }
           if(j%2==1)
           {   if(k==7||k==8)
               image[j][k]=n;
               else
               image[j][k]=0;
           }
      }
   }
   for(i=1;i<c+1;i++) image[9][i]=0;
  break ;    }
  case 2 :
{ do { cout << "\nEnter the patch size (2-6): ";
  cin >> patchsize;}while(patchsize<2||patchsize>7);
  r=9;c=9+2*patchsize;
  do{ cout<< "\nEnter the value of the unknown colour on the grayscale (0-255): ";
     cin >> n;}while(n<=0||n>=255);
  for(i=1;i<c+1;i++)
  image[1][i]=0;
  for(j=2;j<r+1;j++)
   {  patchnum1=patchnum2=0;
      for(k=1;k<c+1;k++)
      {
           if(j%2==0)
           {   if(k==3+patchnum1&&patchsize!=0)
               {  image[j][k]=n;
                  if(patchnum1<patchsize-1)
                  patchnum1++;
               }
               else
               image[j][k]=255;
           }
           if(j%2==1)
           {   if(k==10+patchnum2&&patchsize!=0)
               {  image[j][k]=n;
                  if(patchnum2<patchsize-1)
                  patchnum2++;
               }
               else
               image[j][k]=0;
           }
      }
   }
   for(i=1;i<c+1;i++) image[9][i]=0;
   break;

}
case 3 : {r=9;c=10,n=100;
  for(i=1;i<c+1;i++)
  image[1][i]=0;
  for(j=2;j<r+1;j++)
   {  for(k=1;k<c+1;k++)
      {    if(j%2==0)
           {   if(k==3||k==4)
               image[j][k]=n;
               else
               image[j][k]=255;
           }
           if(j%2==1)
           {   if(k==7||k==8)
               image[j][k]=n;
               else
               image[j][k]=0;
           }
      }
   }
	 						for(i=1;i<c+1;i++) image[6][i]=image[5][i];
	  	  	 for(i=1;i<c+1;i++) image[3][i]=image[4][i];

 						   				for(i=1;i<c+1;i++) image[1][i]=image[4][i];
         	 				  		 	 	 for(i=1;i<c+1;i++) image[7][i]=image[5][i];
         	 				  		 	 	 for(i=1;i<c+1;i++) image[8][i]=image[5][i];

         	 				  		 	 	 for(i=1;i<c+1;i++) image[9][i]=image[5][i];
  break ;    }
  case 4:
  		 { r=9;c=10,n=100;
			  for(i=1;i<c+1;i++)
			  image[1][i]=0;
			  for(j=2;j<r+1;j++)
			  { for(k=1;k<c+1;k++)
				  {
				  	  if(j%2==0)
					  { if(k==3||k==4) image[j][k]=n;  }
     				  	else
				        { if(k==7||k==8) image[j][k]=n;
                    }
				  }
			  }
			  image[r][7]=0; image[r][8]=0;
			  for (i=1;i<r+1;i++)
			  		{ for (j=1;j<6;j++)
			  		  		{ if (image[i][j]!=n) { image[i][j]=255; } }
					}

		break; }
		  case 5:
 		 { cout << "\nEnter the number of rows: " ;
           cin >> r;
 		   cout << "\nEnter the number of columns: " ;
           cin >> c;
 		 	for (i=1;i<r+1;i++)
 		 		 { for (j=1;j<c+1;j++)
 		 		 		 {  do{
	 					 	 	 cout << "Enter the grayscale value of point " << i << ", " << j << " (0-255): ";
	 		 		 		 	 cin >> image[i][j];
	 		 		 		 	 if (image[i][j]<0||image[i][j]>255) cout << "Error. Please enter a value between 0 and 255." << endl;
			 		 			 }while(image[i][j]<0||image[i][j]>255);
						 }
				 }
				 break;
			 }
		case 6: {   BMP InputA; //char inproute[1000];
    BMP Input;
    InputA.SetSize(1000,1000);
    //cout << "Enter the path of the file to open: "; cin >> inproute;
    InputA.ReadFromFile("Input.bmp");
    Input.SetSize(InputA.TellHeight(),InputA.TellWidth());
    Input.ReadFromFile("Input.bmp");

    // convert each pixel to grayscale using RGB->YUV
    for( int i=0 ; i < Input.TellHeight() ; i++)
    {
    for( int j=0 ; j < Input.TellWidth() ; j++)
         { image[i+1][j+1]=Input(j,i)->Red; } }
    r=Input.TellHeight();
    c=Input.TellWidth();

    cout << "\nDo you want to enter a reference point?Y-1.N-2.";
    cin >> point_opt;
    if(point_opt==1)
    {   cout << "\nEnter co-ordinate of point (x,y): " ;
        cin  >>  pt_x >> pt_y;
        pt_x+=1;pt_y+=1;
    }
    else
    cout << "\nWarning.Unstable program!";
    break; }

  /*  case 7: {	r=9;c=10,n=100;
  for(i=1;i<c+1;i++)
  image[1][i]=0;
  for(j=2;j<r+1;j++)
   {  for(k=1;k<c+1;k++)
      {    if(j%2==0)
           {   if(k==3||k==4)
               image[j][k]=n;
               else
               image[j][k]=200;
           }
           if(j%2==1)
           {   if(k==7||k==8)
               image[j][k]=n;
               else
               image[j][k]=0;
           }
      }
   }
   for(i=1;i<c+1;i++) image[9][i]=0;
  break ;  }   */
  }

  for(j=0;j<c+2;j++)
  image[0][j]=image[r+1][j]=255;
  for(i=0;i<r+2;i++)
  image[i][0]=image[i][c+1]=255;
   for(i=0;i<r+2;i++)
  {  for(j=0;j<c+2;j++)
     {  rect_image[i][j]=image[i][j];
     }
  }
  for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {  para[i][j]=0;
     }
  }
while(1)
{ sum=0;p=-1; //initially set to 0/-1
  for(i=0;i<1000000;i++)
  point[i].wc=point[i].bc=0;//initially set to 0
  for(i=0;i<1000000;i++)
  count[i]=avg[i]=sym[i]=0;//initially set to 0
  for (i=0;i<1000000;i++) wc[i]=bc[i]=ival[i]=length[i]=0;
  for(i=0;i<r+2;i++)
  {  for(j=0;j<c+2;j++)
     shadow[i][j]=shadow_layer[i][j]=0;//initially set to 0
  }
  for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {   if(choice==4&&(illusion[i][j]>rect_image[i][j])&&para[i][j]!=3)
         { image[i][j]--;
           if(para[i][j]!=2)
           para[i][j]=1;
           else para[i][j]=3;}
         if(choice==4&&(illusion[i][j]<rect_image[i][j])&&para[i][j]!=3)
         { image[i][j]++;
            if(para[i][j]!=1)
           para[i][j]=2;
           else para[i][j]=3;}
         if(choice==4&&(illusion[i][j]==rect_image[i][j])&&para[i][j]!=3)
         { para[i][j]=3;}
     }
  }
  // para is 1 if less, 2 if greater,3 if equal or value crossed
  flag2=1;//initially set to 1
  flag1=1;//initially set to 1

if(!(opt==6&&point_opt==1))
{ for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {  if((image[i][j]!=0&&image[i][j]!=255)&&shadow[i][j]==0)
        {
           patch(image,shadow,i,j,flag1);
           flag1++;
        }
     }
  }
  for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {  if((image[i][j]!=0&&image[i][j]!=255)&&shadow_layer[i][j]==0)
        {  patch_layer(image,shadow_layer,i,j,flag2);
           flag2++;
        }
     }
  }
}
else
{ point_patch(image,shadow,pt_x,pt_y,flag1,image[pt_x][pt_y]);
  flag1++;
  for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {  if(shadow_layer[i][j]==flag1-1)
         {   point_patch_layer(image,shadow_layer,i,j,flag2,image[pt_x][pt_y]);
             flag2++;
         }
     }
  }
}
if(!(opt==6&&point_opt==1))
{ for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      {  sum_wc=sum_bc=0;
         if(image[i][j]!=0&&image[i][j]!=255)
         {  p++;point[p].x=i;point[p].y=j;
            for(diff1=-1;diff1<=1;diff1++)
            {  for(diff2=-1;diff2<=1;diff2++)
               {  if(!((image[i+diff1][j+diff2]>=(image[i][j]/10)*10)&&(image[i+diff1][j+diff2]<(image[i][j]/10+1)*10)))
                  {   if(image[i+diff1][j+diff2]>image[i][j])
                      sum_wc+=image[i+diff1][j+diff2];
                      if(image[i+diff1][j+diff2]<image[i][j])
                      sum_bc+=(255-image[i+diff1][j+diff2]);
                  }
                  if(diff1==diff2&&diff1==1)
                  {  point[p].wc=sum_wc/255;
                     point[p].bc=sum_bc/255;
                  }
               }
            }
         }
      }
  }
}
else
{   for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      {  sum_wc=sum_bc=0;
         if(image[i][j]!=0&&image[i][j]!=255)
         {  p++;point[p].x=i;point[p].y=j;
            for(diff1=-1;diff1<=1;diff1++)
            {  for(diff2=-1;diff2<=1;diff2++)
               {  if(shadow[i+diff1][j+diff2]!=shadow[i][j])
                  {   if(image[i+diff1][j+diff2]>image[i][j])
                      sum_wc+=image[i+diff1][j+diff2];
                      if(image[i+diff1][j+diff2]<image[i][j])
                      sum_bc+=(255-image[i+diff1][j+diff2]);
                  }
                  if(diff1==diff2&&diff1==1)
                  {  point[p].wc=sum_wc/255;
                     point[p].bc=sum_bc/255;
                  }
               }
            }
         }
      }
  }
}



  { for (m=1;m<flag2;m++)
    { p=-1;//reset to -1
      for (i=1;i<r+1;i++)
      { for(j=1;j<c+1;j++)
        {  if(image[i][j]!=0&&image[i][j]!=255)
           p++;
           if(shadow_layer[i][j]==m)
           {
             { wc[m]+=point[p].wc;
           	   bc[m]+=point[p].bc;
             }
		   }
         }
      }
    }
  }
for (m=1;m<flag2;m++)
  {
     { for (i=1;i<r+1;i++)
       { if(length[m]==0)
         {  for(j=1;j<c+1;j++)
            {  if(shadow_layer[i][j]==m)
               length[m]++;
            }
         }
       }
     }
  }
  int check=0;
  for (m=1;m<flag2;m++)
  {
  { for (i=1;i<r+1;i++)
    {   check=0;
        for(j=1;j<c+1;j++)
         {  if(shadow_layer[i][j]==m)
            {  for(diff1=-1;diff1<=1;diff1++)
               {  for(diff2=-1;diff2<=1;diff2++)
                  {  if(image[i+diff1][j+diff2]==image[i+diff1][j+length[m]-1-diff2])
                     check++;
                  }
               }
               if(check==9&&(sym[m]==1||sym[m]==0))
               sym[m]=1;//symmetric
               else sym[m]=2;//asymmetric
               break;
            }
         }
    }
  } }
  for (m=1;m<flag2;m++)
  {
	 	{ 	 double max=(wc[m]>bc[m]?wc[m]:bc[m]);
		    double min=(wc[m]<bc[m]?wc[m]:bc[m]);
		    double maxrange,minrange;
		    if(length[m]==2)
		    maxrange=2.49999999999;
		    else
		    maxrange=length[m]-2/3;
		    if(length[m]%2==0)
		    minrange=(3*length[m]+2)/(3*length[m]) ;
		    else
		    minrange=3*(length[m]-length[m]/2)/(1+3*length[m]/2) ;
		    if (min==0&&max!=0) ival[m] = -1;//sbc
		    else if (((int)(max/min)==max/min&&(int)(max/min)%3==0&&sym[m]==1))
            ival[m]=1;//white's illusion
            else if((min!=0)&&(max/min<minrange)||(max/min>maxrange))
            ival[m]=-1;//sbc
		    else ival[m]=1;//white's illusion
	 }
  }
  p=-1;
  for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      {  if(image[i][j]!=0&&image[i][j]!=255)
         {  p++;
            if(shadow[i][j]!=0)
            illusion[i][j]=(whitecalculator(image[i][j],point[p].wc,ival[shadow_layer[i][j]])+blackcalculator(image[i][j],point[p].bc,ival[shadow_layer[i][j]]))/2.0;
         }
         else illusion[i][j]=image[i][j];
      }
  }
  for(k=1;k<flag1;k++,sum=0)
  { for (i=1;i<r+1;i++)
    { for (j=1;j<c+1;j++)
      { if (shadow[i][j]==k)
        {count[k]++; sum+=illusion[i][j];}
      }
      avg[k]=int(sum*1.0/(count[k]*1.0));
    }
  }
  for (k=1;k<flag1;k++)
  { for (i=1;i<r+1;i++)
    { for (j=1;j<c+1;j++)
      { if (shadow[i][j]==k)
        { illusion[i][j]=avg[k];
        }
      }
    }
  }
  int flag_rect=0;
  for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {  if(para[i][j]!=3)
        flag_rect=1;
     }
  }

  if((choice==4&&flag_rect==0)||choice!=4)
{  cout <<"\nThe image inputted is: "<<endl;
  for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      cout << image[i][j]<<"\t";
      cout <<endl ;
  }
  //cout << "\nBc:" << bc[1]<<" Wc:"<<wc[1]<<" Length: "<<length[1]<<"\nSymmetry factor: "<<sym[1];
  cout <<"\nThe image is perceived as: "<<endl;
  for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      cout << illusion[i][j]<<"\t";
      cout <<endl ;
  }
//  cout << "\nWC:"<<point[0].wc;
/*cout <<"\nThe shadow patch is: "<<endl;
  for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      cout << shadow[i][j]<<"\t";
      cout <<endl ;
  }
  cout <<"\nThe shadow layering is: "<<endl;
  for(i=1;i<r+1;i++)
  {   for(j=1;j<c+1;j++)
      cout << shadow_layer[i][j]<<"\t";
      cout <<endl ;
  }
  */
  RlImg.SetSize(1*(c),1*(r));
  RlImg.SetBitDepth(32);
  for (i=0; i < r; i++)
  { for (j=0; j < c; j++)
    {  for (diff1=0;diff1<1;diff1++)
       {  for (diff2=0;diff2<1;diff2++)
          { RlImg(j*1+diff1,i*1+diff2)->Red = image[i+1][j+1];
            RlImg(j*1+diff1,i*1+diff2)->Green = image[i+1][j+1];
            RlImg(j*1+diff1,i*1+diff2)->Blue = image[i+1][j+1];
            RlImg(j*1+diff1,i*1+diff2)->Alpha = image[i+1][j+1];
          }
       }
    }
  }
  RlImg.WriteToFile("Processed_Input.bmp");
  cout << endl << "The image representing the inputted image has been created. It is located in the same folder with the filename \"Processed_Input.bmp\"\n";

  cout << endl << "Menu:\n";
  cout << "\n1. Modify a row\n2. Modify a column";
  if (opt==1||opt==3||opt==4) cout << "\n3. Proceed to next intermediate picture";
  cout << "\n4. Rectify image\n5. Enhance image\n0. Terminate program\nEnter your choice: ";
  cin >>choice; int optchoice = 0;
  if (opt==1&&choice==3&&flagconv_1a==0&&flagconv_1b==0) { cout << "1. Approach 1\n2. Approach 2 (proceeds to #4 SBC)\nChoose an option: ";
  cin >> optchoice; }
  optchoice%=3;
  if (flagconv_1a!=0) optchoice = 1;
  if (flagconv_1b!=0) optchoice = 2;
  if (choice==3)
  	  { if (opt==1)
					{ if (optchoice==1)
					  	  { switch(flagconv_1a)
	 							 { case 0:  { for(i=1;i<c+1;i++) image[6][i]=image[5][i];
 						                            reset_array(rect_image,image,para,r,c);
 						   							flagconv_1a++; break;
					     					}
   		 	  	 				case 1: { for(i=1;i<c+1;i++) image[6][i]=image[4][i];
										  	  	 for(i=1;i<c+1;i++) image[3][i]=image[4][i];
 						   					  reset_array(rect_image,image,para,r,c);
 						   					  	flagconv_1a++; break;
								  			  }
  	    						  case 2: {  for(i=1;i<c+1;i++) image[1][i]=image[4][i];
         	 				  		 	 	 for(i=1;i<c+1;i++) image[6][i]=image[5][i];
         	 				  		 	 	 for(i=1;i<c+1;i++) image[7][i]=image[5][i];
         	 				  		 	 	 for(i=1;i<c+1;i++) image[8][i]=image[5][i];

         	 				  		 	 	 for(i=1;i<c+1;i++) image[9][i]=image[5][i];
		 			 						  	reset_array(rect_image,image,para,r,c);
		 				 						 flagconv_1a++;
									 			 }
									   default:  { opt=3; flagconv_1a=0; }
										}
					}
					    else
					    		{  switch(flagconv_1b)
										 { case 0: { for(i=1;i<c+1;i++) { if (i%2==1) image[i][3]=255; }
									  	 		  	  	 for(i=1;i<c+1;i++) { if (i%2==0) image[i][8]=0; }
					     	 						   reset_array(rect_image,image,para,r,c);
 						   							flagconv_1b++; break;
					     								}
											case 1:  { for(i=1;i<c+1;i++) { if (i%2==1) image[i][4]=255; }
									  	 		  	  	 for(i=1;i<c+1;i++) { if (i%2==0) image[i][7]=0; }
					     	 						   reset_array(rect_image,image,para,r,c);
 						   							flagconv_1b++; break;
					     								}
   								      case 2:  { for(i=1;i<c+1;i++) { if (i%2==1) image[i][2]=255; }
									  	 		  	  	 for(i=1;i<c+1;i++) { if (i%2==0) image[i][9]=0; }
					     	 						   reset_array(rect_image,image,para,r,c);
 						   							flagconv_1b++; break;
					     								}
   								       case 3:
										 		     { for(i=1;i<c+1;i++) { image[i][1]=255; image[i][5]=255; }
									  	 		  	  	 for(i=1;i<c+1;i++) { image[i][10]=0;  image[i][6]=0; }
					     	 						   reset_array(rect_image,image,para,r,c);
 						   							flagconv_1b++;
					     								}

									   default:  { opt=4; flagconv_1b=0; }
										}
										}
										 }
				 else if (opt==3)
				 		{ switch(flagconv_3)
					  	     { case 0:		{ for(i=1;i<c+1;i++) image[1][i]=image[9][i]=0;
					  	     	 				  for(i=1;i<c+1;i++) image[8][i]=image[2][i];
					  	     	 				  for(i=1;i<c+1;i++) image[6][i]=image[2][i];
			  						           reset_array(rect_image,image,para,r,c);
                                                 flagconv_3++; break;
											  }
					         case 1:		{ for(i=1;i<c+1;i++) image[6][i]=image[5][i];
					         	  			  for(i=1;i<c+1;i++) image[3][i]=image[5][i];
					         	  			  reset_array(rect_image,image,para,r,c);
                                                 flagconv_3++; break;
											  }
					        case 2:	  { for(i=1;i<c+1;i++) image[6][i]=image[4][i];
					        		 		  	 reset_array(rect_image,image,para,r,c);
                                                    flagconv_3++;
				  						     }
								default: {opt = 1; flagconv_3=0; }
								} }


				else if (opt==4)
				 		{ switch(flagconv_4)
					        { case 0:  	{ for(i=1;i<c+1;i++) { if (i%2==1) image[i][1]=0; }
					        	 				  for(i=1;i<c+1;i++) { if (i%2==1) image[i][5]=0; }
  	 		  	  	 		  	 				  for(i=1;i<c+1;i++) { if (i%2==0) image[i][10]=255; }
  	 		  	  	 		  	 				  for(i=1;i<c+1;i++) { if (i%2==0) image[i][6]=255; }
  	 						   				  reset_array(rect_image,image,para,r,c);
			   								  flagconv_4++; break;
											   }
							    case 1:   { for(i=1;i<c+1;i++) { if (i%2==1) image[i][2]=0; }
  	 		  	  	 		  	 				  for(i=1;i<c+1;i++) { if (i%2==0) image[i][9]=255; }
  	 						   				  reset_array(rect_image,image,para,r,c);
			   								  flagconv_4++; break;
											   }
				             case 2:    { for(i=1;i<c+1;i++) { if (i%2==1) image[i][4]=0; }
  	 		  	  	 		  	 				  for(i=1;i<c+1;i++) { if (i%2==0) image[i][7]=255; }
  	 						   				  reset_array(rect_image,image,para,r,c);
			   								  flagconv_4++; break;
											   }
							    case 3:		{ for(i=1;i<c+1;i++) { if (i%2==1) image[i][3]=0; }
  	 		  	  	 		  	 				  for(i=1;i<c+1;i++) { if (i%2==0) image[i][8]=255; }
  	 						   				  reset_array(rect_image,image,para,r,c);
			   								  flagconv_4++;
											   }
							      default:  { opt=1; flagconv_4=0; }
				 } } }

  if(choice==1)
  {  cout << "\nEnter the number of the row you wish to modify: ";
  	  opt = 42; //wut?
     cin >> mod;
     for(j=1,k=0;j<c+1;j++,k=0)
     {  do { if (k==1) cout << "\nError, enter 0, " << n << ", or 255" << endl;
             cout << "\nEnter value (row " <<mod<<") (column "<<j<<") (0, " << n <<", or 255): ";
             cin >> image[mod][j];
             k++;
           }while(image[mod][j]!=0&&image[mod][j]!=255&&image[mod][j]!=n);
     }
     reset_array(rect_image,image,para,r,c);
  }
  if(choice==2)
  {  cout << "\nEnter the number of the column you wish to modify: ";
     cin >> mod;
     opt = 42;  //wut?
     for(i=1,k=0;i<r+1;i++,k=0)
     {  do { if (k==1) cout << "\nError, enter 0, " << n << ", or 255" << endl;
             cout << "\nEnter value (row " <<i<<" ) (column "<<mod<<") (0, " << n << ", or 255): ";
             cin >> image[i][mod];
             k++;
           }while(image[i][mod]!=0&&image[i][mod]!=255&&image[i][mod]!=n);
     }
     reset_array(rect_image,image,para,r,c);
  }
  if(choice==5)
  {   for(i=0;i<r+2;i++)
      {  for(j=0;j<c+2;j++)
         {  image[i][j]=illusion[i][j];
         }
      }
      reset_array(rect_image,image,para,r,c);
  }
  if(choice==0)
  exit(0);
}}
  fflush(stdin);
  getchar();
}
int whitecalculator(int n,int wc,int i)
{   if(wc!=0)
    { int xx= ((n+(n-255)/4.0)*pow((wc*1.0),(i*0.15))) ;
      if(xx< 0)
      return 0;
      else
      return xx;
    }
    else
    return n;
}
int blackcalculator(int n,int bc,int i)
{   if(bc!=0)
    { int xx= ((n+(n-0)/4.0)*pow((bc*1.0),(-i*0.15))) ;
      if(xx> 255)
      return 255;
      else
      return xx;
    }
    else
    return n;
}
void patch(int img[1000][1000],int arr[1000][1000],int x,int y,int flag)
{ int diff1,diff2;
  arr[x][y]=flag;
  for(diff1=-1;diff1<=1;diff1++)
  {  for(diff2=-1;diff2<=1;diff2++)
     {  if(((img[x+diff1][y+diff2]>=(img[x][y]/10)*10)&&(img[x+diff1][y+diff2]<(img[x][y]/10+1)*10))&&arr[x+diff1][y+diff2]==0)
        patch(img,arr,x+diff1,y+diff2,flag);
     }
  }
  return ;
}
void patch_layer(int img[1000][1000],int arr[1000][1000],int x,int y,int flag)
{
  arr[x][y]=flag;
  {
     {  if(((img[x][y+1]>=(img[x][y]/10)*10)&&(img[x][y+1]<(img[x][y]/10+1)*10))&&arr[x][y+1]==0)
        patch_layer(img,arr,x,y+1,flag);
     }
  }
  return ;
}
void reset_array(int rect_image[1000][1000],int image[1000][1000],int para[1000][1000],int r,int c)
{ int i,j;
  for(i=0;i<r+2;i++)
  {  for(j=0;j<c+2;j++)
     {  rect_image[i][j]=image[i][j];
     }
  }
  for(i=1;i<r+1;i++)
  {  for(j=1;j<c+1;j++)
     {  para[i][j]=0;
     }
  }
}
void point_patch(int img[1000][1000],int arr[1000][1000],int x,int y,int flag, int ref_point)
{ int diff1,diff2;
  arr[x][y]=flag;
  for(diff1=-1;diff1<=1;diff1++)
  {  for(diff2=-1;diff2<=1;diff2++)
     {  if(((img[x+diff1][y+diff2]>=ref_point-10)&&(img[x+diff1][y+diff2]<ref_point+10))&&arr[x+diff1][y+diff2]==0)
        point_patch(img,arr,x+diff1,y+diff2,flag,ref_point);
     }
  }
  return ;
}
void point_patch_layer(int img[1000][1000],int arr[1000][1000],int x,int y,int flag,int ref_point)
{
  arr[x][y]=flag;
  {
     {  if(((img[x][y+1]>=ref_point-10)&&(img[x][y+1]<ref_point+10))&&arr[x][y+1]==0)
        point_patch_layer(img,arr,x,y+1,flag,ref_point);
     }
  }
  return ;
}


